from pyquery import PyQuery as pq
import requests
import pprint
from pybloom import BloomFilter
from urllib.parse import urljoin
from urllib.parse import urlparse
from urllib.parse import urlencode
from boilerpipe.extract import Extractor

initialRoot = 'http://www.e-ziare.ro/index.php?z=ziare-a-z'
MAX_PAGES = 1000

def extractPapers(root):
	results = []
	html = requests.get(initialRoot).text
	dom = pq(html)
	elements = dom('a[target]')
	for element in elements:
		link = pq(element).attr('href')
		if link.startswith('/'):
			continue
		item = {}
		if not link.startswith('http'):
			item['url'] = initialRoot + link
		else:
			item['url'] = link
		results.append(item)
	return results

def extractArticles(paperLink):
	f = BloomFilter(capacity=1000, error_rate=0.001)
	status = {}
	status['pageCount'] = 0
	status['articleCount'] = 0
	processLink(paperLink, status)

def processLink(link, status):
	html = requests.get(link).text
	dom = pq(html)
	elements = dom('a')
	for element in elements:
		link = pq(element).attr('href')
		if not isAdded(link, f):
			status['pageCount'] = status['pageCount'] + 1
			if isArticle(element):
				status['articleCount'] = status['articleCount'] + 1
				text = extractText(link)
			if status['pageCount'] < MAX_PAGES:
				processLink(link, status)

def isAdded(link, f):
	return f.add(link)

def isArticle(element):
	if len(pq(element).text().split(' ')) > 9:
		return True
	return False

def extractText(link):
	extractor = Extractor(extractor='ArticleExtractor', url=link)
	return extractor.getText()

def main():
	# results = extractPapers(initialRoot)
	# pprint.pprint(results)
	# sampleLink = 'http://www.romanialibera.ro/actualitate/eveniment/casa-elenei-basescu-a-fost-sparta--principalul-suspect--arestat-390800'
	samplePaper = 'http://adevarul.ro'
	# text = extractText(sampleLink)
	# pprint.pprint(text)

if __name__ == '__main__':
	main()