import xml.etree.ElementTree as ET
import glob

def convert_xml_txt(filename,typex):
    tree = ET.parse(filename)
    root = tree.getroot()

    f = open(filename + ".txt", "wb")

    for propozitie in root:
        for tok in propozitie:
            for child in tok:
                if child.tag == 'orth':
                    f.write(child.text.encode('utf-8'))
                    f.write('/')
                elif child.tag == 'ctag' and typex == 'parte_de_vorbire':
                    f.write(child.text.encode('utf-8'))
                    f.write(' ')
                elif child.tag == 'syn' and typex == 'parte_de_propozitie':
                    f.write(child.find('reltype').text.encode('utf-8'))
                    f.write(' ')
        f.write('\n\n')
    
    f.close()

tfiles = glob.glob('t*.xml')
tpfiles = glob.glob('tp*.xml')

for f in tfiles:
    print "Converting t*.xml",f
    convert_xml_txt(f,'parte_de_vorbire')
for f in tpfiles:
    print "Converting tp*.xml",f
    convert_xml_txt(f,'parte_de_propozitie')