# Romanian NLTK

The purpose of this project is to build a Natural Language Processing system for the Romanian language.

The ultimate purpose of this system is to be used inside an application. The application receives as *input* a declaration from a Romanian public figure (i.e. politician). It produces, as *output*, a measure of the veridicality of the declaration.