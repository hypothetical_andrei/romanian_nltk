from pyquery import PyQuery as pq
import requests
from nltk import FreqDist
from nltk import word_tokenize
import nltk
from nltk.tokenize import RegexpTokenizer
import os
import uuid
import json
import time

import pprint
import locale


locale.getpreferredencoding = lambda:'UTF-16'


def doWork(seed):
	results = []
	html = ''
	try:
		html = requests.get(seed['url']).content
		dom = pq(html)
		dom.find('script,style').remove()
		raw = dom('html').text()
	except Exception as e:
		pprint.pprint('xml or requests exception on : ' + seed['url'])
		return results
	# cwd = os.getcwd()
	# d = cwd + '/output'
	# if not os.path.exists(d):
	# 	os.makedirs(d)

	# toker = RegexpTokenizer(r'((?<=[^\w\s])\w(?=[^\w\s])|(\W))+', gaps=True)
	# tokens = toker.tokenize(raw)
	# text = nltk.Text(tokens)
	# fdist = FreqDist(text)
	# results.append(fdist)

	# with open(d + '/' + str(uuid.uuid1()),"w") as f:
	# 	f.write(json.dumps(fdist))
	return results