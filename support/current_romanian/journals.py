from pyquery import PyQuery as pq
import requests
import pprint

def doWork(seed):
	initialRoot = 'http://www.e-ziare.ro/'
	results = []
	html = requests.get(initialRoot).text
	# pprint.pprint(html)
	dom = pq(html)
	elements = dom('a[target]')
	# print(str(len(elements)))
	for element in elements:
		link = pq(element).attr('href')
		if link.startswith('/'):
			continue
		item = {}
		if not link.startswith('http'):
			item['url'] = initialRoot + link
		else:
			item['url'] = link
		results.append(item)
	return results