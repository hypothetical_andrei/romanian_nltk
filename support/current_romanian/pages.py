from pyquery import PyQuery as pq
import requests
from langdetect import detect_langs
import pprint
import locale

locale.getpreferredencoding = lambda:'UTF-16'


def doWork(seed):
	initialRoot = seed['url']
	results = []
	raw = ''
	try:
		response = requests.get(initialRoot)
		homeUrl = response.url
		html = response.content
		dom = pq(html)
		dom.remove('script, style')
		# raw = dom.text()
	except Exception as e:
		pprint.pprint('http or xml exception on : ' + seed['url'])
		return results
	# langs = []
	# # pprint.pprint(raw)
	# try:
	# 	if raw:
	# 		langs = detect_langs(raw)
	# except Exception as e:
	# 	pprint.pprint('lang exception on : ' + seed['url'])
	# lang = ''
	# if langs and len(langs) > 0:
	# 	lang = langs[0].lang
	# if not lang == 'ro':
	# 	return results
	elements = dom('a[href]')
	for element in elements:
		link = pq(element).attr('href')
		item = {}
		if not link.startswith('http'):
			item['url'] = homeUrl + link
		else:
			item['url'] = link
		if homeUrl in link:
			results.append(item)
	pprint.pprint(results)		
	return results